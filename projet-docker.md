# Projet Docker LP 2018

Le projet docker a faire consiste à "Dockeriser" un de vos projets et si possible à y ajouter une intégration continue dans Gitlab.
Il peut s'agir de votre projet:

- Node
- PHP
- Polymer
- Tutoré
- Android
- Kivy

## Configuration Docker

Il faudra donc produire une configuration de Docker **DockerFile**, **docker-compose.yml**, etc. opérationnelle pour faire tourner votre projet.
Il faut distinguer au moins 2 services différents dans votre projet comme par exemple un mongodb et une app node.

## Intégration  continue

Si possible, ajouter une configuration **gitlab-ci.yml** d'intégration continue à votre dépôt Gitlab permettant de lancer des tests d'intégration continue (*phpunit*, python *unittest*, *mocha/chai* pour nodejs, *junit* pour java, *selenium*, etc),  génération de documentation (*doxygen, sami, jsdoc, docco*, etc.) taux de couverture ou autres comme dans l'exemple fourni dans Celene. Les badges correspondants peuvent ensuite être affichés dans le *Readme.md*:

```md
[![build status](https://gitlab.com/bob/project/badges/master/build.svg)](https://gitlab.com/bob/project/commits/master) [![coverage report](https://gitlab.com/bob/project/badges/master/coverage.svg)](https://bob.gitlab.io/project/coverage)
```

```
Le projet est à rendre au plus tard le **17 Février**. Il faut rendre un dépôt gitlab contenant les fichiers nécessaires au lancement de votre image (Dockerfile, docker-compose.yml) et un Readme.md contenant des explications. Le projet doit être directement utilisable sous Linux.
Veuillez ajouter à votre dépôt votre enseignant de TD.
```


